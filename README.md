# Create Categories from Json (Graph)

## Install

* git clone https://github.com/dcroid/tencoins.git

* pip install -f requirements.txt

* Edit settings.py, add your db data (host, user, password, db name, port)

* Migrate in db "python manage.py migrate"