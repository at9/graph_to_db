# Generated by Django 2.0.4 on 2018-04-30 16:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('adjacency', '0002_auto_20180430_1607'),
    ]

    operations = [
        migrations.RenameField(
            model_name='categoryid',
            old_name='data_json',
            new_name='data',
        ),
    ]
