# Generated by Django 2.0.4 on 2018-04-30 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adjacency', '0007_auto_20180430_2156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorymodel',
            name='father',
            field=models.IntegerField(blank=True, null=True, verbose_name='Father'),
        ),
    ]
